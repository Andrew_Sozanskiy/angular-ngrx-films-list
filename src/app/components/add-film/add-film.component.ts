import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';

import { Film } from '../../models';

@Component({
  selector: 'app-add-film',
  template: `
    <form #f=ngForm (ngSubmit)="onSubmit(f)" novalidate class="example-form">
      <mat-form-field class="example-full-width">
        <input matInput [(ngModel)]="filmName" name="Name" placeholder="Film Name" required>
      </mat-form-field>

      <mat-form-field class="example-full-width">
        <textarea matInput [(ngModel)]="description" name="description" placeholder="Description"></textarea>
      </mat-form-field>

      <mat-form-field class="example-full-width">
        <input matInput [(ngModel)]="imageUrl" name="imageUrl" placeholder="Image Url">
      </mat-form-field>
      <button mat-flat-button color="primary">Add</button>
    </form>
  `,
  styles: [
    `
    .example-form {
      min-width: 150px;
      max-width: 500px;
      width: 100%;
    }
    .example-full-width {
      width: 100%;
    }`
  ]
})
export class AddFilmComponent implements OnInit {
  @Output()
  addFilm = new EventEmitter();

  filmName: string;
  description: string;
  imageUrl: string;
  constructor() {}

  ngOnInit() {}

  onSubmit(f: NgForm) {
    // tslint:disable-next-line:curly
    if (f.invalid) return;

    const film: Film = {
      id: 0,
      name: this.filmName,
      img: '',
      description: this.description
    };

    this.addFilm.emit(film);

    f.resetForm();
  }
}
