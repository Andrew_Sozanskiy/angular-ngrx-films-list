import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output
} from '@angular/core';
import { MatDialog } from '@angular/material';

import { Film } from '../../models';
import { CreateFilmModalComponent } from '../../shared/modals/create-film-modal/create-film-modal.component';

@Component({
  selector: 'app-film-list',
  template: `
    <mat-list role="list">
      <mat-list-item  *ngFor="let film of films; let i = index" role="listitem" (click)="select.emit(film.id)">
        <app-film-item  [film]="film"></app-film-item>
        <button mat-flat-button (click)="remove.emit(i, film.id)">Remove</button>
      </mat-list-item>
    </mat-list>
    <button mat-flat-button color="primary" (click)="addFilm()">Add Film</button>
  `,
  styles: [
    `
      mat-list-item:hover {
        background-color: #ccc;
      }
    `
  ]
})
export class FilmListComponent implements OnInit, OnChanges {
  @Input()
  films: Film[];
  @Input()
  label: string;
  @Output()
  select = new EventEmitter();
  @Output()
  remove = new EventEmitter();

  constructor(public dialog: MatDialog) {}

  ngOnInit() {}

  ngOnChanges(changes) {
    console.log(changes);
  }

  addFilm() {
    const addFilmRef = this.dialog.open(CreateFilmModalComponent, {
      height: '400px',
      width: '600px'
    });
  }
}
