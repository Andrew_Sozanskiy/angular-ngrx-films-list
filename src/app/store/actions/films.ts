import { Action } from '@ngrx/store';

import { Film } from '../../models';

export const SELECT = '[Films] Select';
export const ADD_ONE = '[Films] Add One';
export const REMOVE_ONE = '[Films] Remove One';

export class Select implements Action {
    readonly type = SELECT;

    constructor(public payload: number) { }
}

export class AddOne implements Action {
    readonly type = ADD_ONE;

    constructor(public payload: Film) { }
}

export class RemoveOne implements Action {
    readonly type = REMOVE_ONE;

    constructor(public payload: number) {}
}

export type Action = AddOne | Select | RemoveOne;


