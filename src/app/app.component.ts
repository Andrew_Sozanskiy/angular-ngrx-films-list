import { Component, OnChanges } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import { Film } from './models';
import * as filmAction from './store/actions/films';
import * as fromRoot from './store/reducers';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnChanges {
  films$: Observable<Film[]>;
  selected$: Observable<Film>;

  constructor(private store: Store<fromRoot.State>) {
    this.films$ = store.select(fromRoot.getAllFilms);
    console.log('this.films', this.films$);
    this.selected$ = store.select(fromRoot.getSelectedFilm);
  }

  ngOnChanges() {
    console.log('this.films$', this.films$);
  }

  onSelect(id: number) {
    this.store.dispatch(new filmAction.Select(id));
  }

  onAdd(film: Film) {
    this.store.dispatch(new filmAction.AddOne(film));
  }

  onRemove(id: number) {
    this.store.dispatch(new filmAction.RemoveOne(id));
  }
}
