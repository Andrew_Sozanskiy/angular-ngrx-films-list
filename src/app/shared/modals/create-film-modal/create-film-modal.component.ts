import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-create-film-modal',
  templateUrl: './create-film-modal.component.html',
  styleUrls: ['./create-film-modal.component.scss']
})
export class CreateFilmModalComponent {
  constructor(
    public dialogRef: MatDialogRef<CreateFilmModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}

export interface DialogData {
  animal: string;
  name: string;
}
