import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateFilmModalComponent } from './create-film-modal.component';

describe('CreateFilmModalComponent', () => {
  let component: CreateFilmModalComponent;
  let fixture: ComponentFixture<CreateFilmModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateFilmModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateFilmModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
